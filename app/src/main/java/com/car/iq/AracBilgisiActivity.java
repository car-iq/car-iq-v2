package com.car.iq;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class AracBilgisiActivity extends AppCompatActivity {

    Button aracsilbuton;
    TextView aracsiltext;
    Button aracsecbuton;
    TextView aracsectext;
    Button araceklebuton;
    TextView aracekletext;
    RelativeLayout arac_ekle_layout;
    SessionManagement session;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arac_bilgisi);

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db=new DatabaseHelper(this);

        session = new SessionManagement(getApplicationContext());

        aracsecbuton=findViewById(R.id.arac_sec_buton);
        aracsectext=findViewById(R.id.arac_sec_text);
        aracsilbuton=findViewById(R.id.arac_sil_buton);
        aracsiltext=findViewById(R.id.arac_sil_text);
        araceklebuton=findViewById(R.id.arac_ekle_buton);
        aracekletext=findViewById(R.id.arac_ekle_text);
        arac_ekle_layout=findViewById(R.id.arac_ekle_layout);

        if(db.aracVarMi()){
            if(db.arac20Mi()){
                /*aracekletext.setVisibility(View.GONE);
                araceklebuton.setVisibility(View.GONE);*/
                arac_ekle_layout.setVisibility(View.GONE);
            }
        }
        else{
            aracsiltext.setVisibility(View.INVISIBLE);
            aracsilbuton.setVisibility(View.INVISIBLE);
            aracsectext.setVisibility(View.INVISIBLE);
            aracsecbuton.setVisibility(View.INVISIBLE);
        }


    }

    public void arac_ekle_buton(View view)
    {
        startActivity(new Intent(this, AracEkleActivity.class));
        this.finish();
    }

    public void arac_sil_buton(View view)
    {
        startActivity(new Intent(this, AracSilActivity.class));
        this.finish();
    }

    public void arac_sec_buton(View view){
        startActivity(new Intent(this, AracSecActivity.class));
        this.finish();
    }
}
