package com.car.iq;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class AteslenmeGrafikActivity extends AppCompatActivity {

    LineChart mpLineChart;//Grafik değişkeni
    TextView tarihAraligi;//Yukarıda belirtilen tarih aralığı değişkeni
    DatabaseHelper db;//Database İşlem sınıfı tanımı
    ArrayList<String> listItem;//Veritabanından çekilen verilerin tutulacağı arraylist
    String[] dataArray;//Arraylist in aktarılacağı dizi
    int[] dataCountArray=new int[30];//Grafiğe veri vermesi için hesaplanmış dizi
    String[] dateArray=new String[30];//Son 30 günün tarihini tutan dizi
    SessionManagement session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ateslenme_grafik);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //Database İşlem sınıfı tanımı
        db=new DatabaseHelper(this);

        //Arraylist tanımı
        listItem=new ArrayList<>();
        listItem.clear();

        //DateArray a son 30 günün ataması
        for(int i=0;i<30;i++)
        {
            dateArray[i]=agoDay(29-i);
        }

        //Veritabanından Son 30 günün verileri alınması
        //getData();

        //Data Count Array sayaç olarak kullanılacak dizisinin elemanlarına başlangıç durumu olarak 0 atanması
        for(int i=0;i<30;i++)
        {
            dataCountArray[i]=0;
        }

        //Data Array içindeki her değerin eşleştiği tarih için sayacın artırılması
        for(int i=0;i<dataArray.length;i++)
        {
            for (int j=0;j<30;j++)
            {
                if(dataArray[i].equals(dateArray[j])){
                    dataCountArray[j]=dataCountArray[j]+1;
                }
            }
        }

        //Itemlerin tanımlanması
        tarihAraligi=findViewById(R.id.tarihAraligi);
        mpLineChart=findViewById(R.id.line_chart);
        LineDataSet lineDataSet=new LineDataSet(dataValues(),"");
        ArrayList<ILineDataSet> dataSets=new ArrayList<>();
        dataSets.add(lineDataSet);

        //Grafik verisi ayarlanması
        LineData data=new LineData(dataSets);
        mpLineChart.setData(data);
        mpLineChart.invalidate();

        //Grafiğin x ve y düzlemleri ayarlanması
        XAxis xAxis=mpLineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelCount(30,true);
        xAxis.setDrawLabels(false);

        YAxis yAxisRight=mpLineChart.getAxisRight();
        YAxis yAxisLeft=mpLineChart.getAxisLeft();

        yAxisRight.setDrawAxisLine(false);
        yAxisRight.setDrawLabels(false);
        yAxisRight.setDrawGridLines(false);
        yAxisRight.setDrawZeroLine(false);

        yAxisLeft.setAxisMinimum(0f);
        yAxisLeft.setAxisMaximum(30f);

        //Tarih aralığı formatı
        tarihAraligi.setText(dateArray[0]+"/"+dateArray[29]);

        //Grafikte sayıların noktalı görünmemesi formatı
        lineDataSet.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return String.valueOf((int)value);
            }
        });

    }

    //Grafiğin Verisinin Eklenmesi
    private ArrayList<Entry> dataValues(){
        ArrayList<Entry> data=new ArrayList<>();
        for(int i=0;i<30;i++)
        {
            data.add(new Entry(i+1,dataCountArray[i]));
        }

        return data;
    }

    //Bulunduğumuz günden belirli bir gün öncesinin tarihini alan fonksiyon
    private String agoDay(int i){
        DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -i);
        return dateFormat.format(cal.getTime());
    }

    //Veritabanından son 30 günün verisini alan fonksiyon
   /* private void getData() {
        Cursor cursor =db.get30Data();
        while (cursor.moveToNext()){
            listItem.add(cursor.getString(0));
        }
        //Alınan verilerin Arraylist ten data array dizisine atanması
        dataArray=new String[listItem.size()];
        for(int i =0;i<dataArray.length;i++)
        {
            dataArray[i]=listItem.get(i);
        }
    }*/


}
