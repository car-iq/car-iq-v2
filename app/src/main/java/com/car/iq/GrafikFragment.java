package com.car.iq;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;


public class GrafikFragment extends Fragment {

    LineChart mpLineChart;//Grafik değişkeni
    float[] dataArray;//Arraylist in aktarılacağı dizi
    String gasName;

    public GrafikFragment() {
        // Required empty public constructor
    }
    public static GrafikFragment newInstance(float[] dataArray, String gasName)
    {
        GrafikFragment grafikFragment=new GrafikFragment();
        Bundle args=new Bundle();
        args.putFloatArray("someArray",dataArray);
        args.putString("someString",gasName);
        grafikFragment.setArguments(args);
        return grafikFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataArray=getArguments().getFloatArray("someArray");
        gasName=getArguments().getString("someString");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grafik, container, false);
        mpLineChart = view.findViewById(R.id.line_chartx);
        co2Ayarlar(mpLineChart);
        return view;
    }


    private ArrayList<Entry> dataValues(){
        ArrayList<Entry> data=new ArrayList<>();
        for(int i=0;i<30;i++)
        {
            data.add(new Entry(i+1,dataArray[i]));
        }

        return data;
    }
    private void co2Ayarlar(LineChart mpLineChart)
    {
        LineDataSet lineDataSet=new LineDataSet(dataValues(),"");
        ArrayList<ILineDataSet> dataSets=new ArrayList<>();
        dataSets.add(lineDataSet);

        //Grafik verisi ayarlanması
        LineData data=new LineData(dataSets);
        mpLineChart.setData(data);
        mpLineChart.invalidate();

        //Grafiğin x ve y düzlemleri ayarlanması
        XAxis xAxis=mpLineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setLabelCount(30,true);
        xAxis.setDrawLabels(false);


        YAxis yAxisRight=mpLineChart.getAxisRight();
        YAxis yAxisLeft=mpLineChart.getAxisLeft();
        yAxisLeft.setLabelCount(6,true);
        yAxisRight.setDrawAxisLine(false);
        yAxisRight.setDrawLabels(false);
        yAxisRight.setDrawGridLines(false);
        yAxisRight.setDrawZeroLine(false);

        //yAxisLeft.setAxisMinimum(0f);
        //yAxisLeft.setAxisMaximum(1.0f);
        mpLineChart.getDescription().setEnabled(false);

        //Grafikte sayıların noktalı görünmemesi formatı
        lineDataSet.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                if(value==0||gasName.equals("co")||gasName.equals("nox")){
                    return String.valueOf((int)value);
                }
                else {
                    return String.valueOf(value);
                }
            }
        });
        lineDataSet.setValueTextSize(13);
    }
}