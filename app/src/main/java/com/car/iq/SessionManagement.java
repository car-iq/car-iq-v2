package com.car.iq;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SessionManagement {
    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "KullaniciPref";
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String PLAKA_SECIM="Secildi";
    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "isim";

    // Phone address (make variable public to access from outside)
    public static final String KEY_PHONE = "telefon";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    // Pass address (make variable public to access from outside)
    public static final String KEY_PASS = "password";

    public static final String KEY_PLAKA= "plaka";

    // Constructor
    public SessionManagement(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String isim,String telefon, String email,String sifre){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        editor.putString(KEY_NAME, isim);

        // Storing email in pref
        editor.putString(KEY_PHONE, telefon);

        // Storing email in pref
        editor.putString(KEY_EMAIL, email);

        // Storing email in pref
        editor.putString(KEY_PASS, sifre);

        // commit changes
        editor.commit();
    }

    public void setPlaka(String plaka){
        editor.putBoolean(PLAKA_SECIM,true);
        editor.putString(KEY_PLAKA,plaka);
        editor.commit();
    }

    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));

        // user email id
        user.put(KEY_PHONE, pref.getString(KEY_PHONE, null));

        // user email id
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));

        // user email id
        user.put(KEY_PASS, pref.getString(KEY_PASS, null));

        // return user
        return user;
    }

    public HashMap<String,String>getPlaka(){
        HashMap<String, String> plaka = new HashMap<String, String>();

        plaka.put(KEY_PLAKA,pref.getString(KEY_PLAKA,null));
        return plaka;

    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public boolean checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(context, KullaniciGirisActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            context.startActivity(i);
            return false;
        }
        return true;

    }
    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

    public boolean plakaSecildiMi(){
        return pref.getBoolean(PLAKA_SECIM,false);
    }

    public boolean checkPlakaSecildi(){
        if(!this.plakaSecildiMi()){
            return false;
        }
        else{
            return true;
        }
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(context, KullaniciGirisActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        context.startActivity(i);
    }
}

