package com.car.iq;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CihazKontrolActivity extends AppCompatActivity {

    private final static String TAG = CihazKontrolActivity.class.getSimpleName();

    private static boolean Connected;
    private static boolean mConnectState;
    private static boolean mServiceConnected;
    private static BLEService bleService;

    private static Switch ble_eslestir;
    private static Switch led_switch;
    private static Switch led_switch2;
    private static Switch button0;
    private static Switch button1;
    private static RelativeLayout loading_panel;
    private static LinearLayout ogeler;
    private static TextView batteryLevel;
    private static TextView currentTime;
    private static TextView currentDate;
    private static TextView batteryColor;

    private static String saatTarihString;
    private static String pilString;

    Handler handler = new Handler();
    Runnable runnable;
    int delay = 500;
    int CNTR = 0;



    private static final int REQUEST_ENABLE_BLE = 1;
    // Request codes
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE = 1;
    private final static int MY_PERMISSIONS_REQUEST_ENABLE_BT = 2;

    //Bu, Android 6.0 (Marshmallow) için gereklidir
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            Log.i(TAG, "onServiceConnected");
            bleService = ((BLEService.LocalBinder) service).getService();
            mServiceConnected = true;
            bleService.initialize();
        }
        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.i(TAG, "onServiceDisconnected");
            bleService = null;


        }
    };
    public void doBindService() {
        Intent gattServiceIntent = new Intent(this, BLEService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cihaz_kontrol);

        ble_eslestir=findViewById(R.id.ble_eslestir);
        led_switch = findViewById(R.id.led_switch);
        led_switch2 = findViewById(R.id.led_switch2);
        button0 = findViewById(R.id.button0);
        button1 = findViewById(R.id.button1);
        loading_panel=findViewById(R.id.loadingPanel);
        ogeler=findViewById(R.id.ogeler);
        batteryLevel=findViewById(R.id.batteryLevel);
        currentTime=findViewById(R.id.currentTime);
        currentDate=findViewById(R.id.currentDate);
        batteryColor=findViewById(R.id.batteryColor);

        // BLE Başlat
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BLE);
        }

        //  Konum Erişimi
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check 
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("This app needs location access ");
                builder.setMessage("Please grant location access so this app can detect devices.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });
                builder.show();
            }
        }

        Intent gattServiceIntent = new Intent(this, BLEService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        if(bleService.baglanti){
            ble_eslestir.setChecked(true);
            led_switch.setEnabled(true);
            led_switch2.setEnabled(true);
            button0.setEnabled(true);
            button1.setEnabled(true);
            ogeler.setVisibility(View.VISIBLE);
            /*try {
                Thread.sleep(250);
                cihazKontrol();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
        }

        ble_eslestir.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Anahtarın durumuna bağlı olarak LED'i açma veya kapatma
                if(isChecked){
                    Ble_Eslestir();
                }
                else {
                    bleService.disconnect();
                }
            }
        });
        led_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Anahtarın durumuna bağlı olarak LED'i açma veya kapatma
                bleService.writeLedCharacteristic(isChecked);
            }
        });
        led_switch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Anahtarın durumuna bağlı olarak LED'i açma veya kapatma
                bleService.writeLed2Characteristic(isChecked);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("Permission for 6.0:", "Coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }
                    });
                    builder.show();
                }
            }
        }
    }

    @Override
    protected void onResume() {
        handler.postDelayed(runnable = new Runnable() {
            public void run() {
                handler.postDelayed(runnable, delay);
                if(Connected) {
                    if(CNTR==0) {
                        bleService.readButtonCharacteristic();
                        CNTR = 1;
                    }
                    else if(CNTR==1) {
                        bleService.readButton2Characteristic();
                        CNTR = 0;
                    }
                }
            }
        }, delay);
        super.onResume();
        if(bleService==null){
            doBindService();
        }
        final IntentFilter filter = new IntentFilter();
        filter.addAction(BLEService.ACTION_BLESCAN_CALLBACK);
        filter.addAction(BLEService.ACTION_CONNECTED);
        filter.addAction(BLEService.ACTION_DISCONNECTED);
        filter.addAction(BLEService.ACTION_SERVICES_DISCOVERED);
        filter.addAction(BLEService.ACTION_DATA_RECEIVED);
        filter.addAction(BLEService.STRING_RECEIVED);
        filter.addAction(BLEService.CONNECTION_FAILED);
        registerReceiver(mBleUpdateReceiver, filter);


        try {
            cihazKontrol();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        super.onResume();
    }
    @Override
    protected void onPause() {
        if(bleService!=null){
            unbindService(mServiceConnection);
            bleService=null;
        }
        unregisterReceiver(mBleUpdateReceiver);
        handler.removeCallbacks(runnable);
        super.onPause();
    }

    public void Ble_Eslestir() {
        if(mServiceConnected) {
            loading_panel.setVisibility(View.VISIBLE);
            bleService.scan();
        }
    }

    public void cihazKontrol(View view) throws InterruptedException {

        bleService.writeReadData("#PIL_SEVIYESI");
        //bleService.writeReadData("#SAAT_BILGISI");
        /*String pil_data=bleService.writeReadData("#PIL_SEVIYESI");
        int pildata=Integer.valueOf(pil_data.substring(16,17)+pil_data.substring(18,19));
        pildata*=2;
        batteryLevel.setText(pildata+"%");

        Thread.sleep(1000);
        String saat_data=bleService.writeReadData("#SAAT_BILGISI");
        String tarih_data=saat_data;
        saat_data=saat_data.substring(14,22);
        currentTime.setText(saat_data);

        //String tarih_data=bleService.writeReadData("#SAAT_BILGISI");
        tarih_data=tarih_data.substring(23);
        currentDate.setText(tarih_data);*/


    }
    synchronized void cihazKontrol2(){

    }
    public void cihazKontrol() throws InterruptedException {

        bleService.writeReadData("#PIL_SEVIYESI");
        //bleService.writeReadData("#SAAT_BILGISI");
        /*synchronized (this){
            String pil_data=bleService.writeReadData("#PIL_SEVIYESI");
            Thread.sleep(1000);
            int pildata=Integer.valueOf(pil_data.substring(16,17)+pil_data.substring(18,19));
            pildata*=20;
            batteryLevel.setText(pildata+"%");
            notifyAll();}

        synchronized (this){
            wait();
            Thread.sleep(1000);
            String saat_data=bleService.writeReadData("#SAAT_BILGISI");
            String tarih_data=saat_data;
            saat_data=saat_data.substring(14,22);
            currentTime.setText(saat_data);
            tarih_data=tarih_data.substring(23);
            currentDate.setText(tarih_data);
        }*/


    }
    private void baglantiBasarisizUyari(){
        Toast.makeText(this,"Bağlantı Başarısız Tekrar Deneyin",Toast.LENGTH_LONG).show();

    }
    public void senkronizasyonButon(View view) throws InterruptedException {
        //Toast.makeText(this,"FABeacon a bağlanılamadı",Toast.LENGTH_SHORT).show();
        String time = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        String sistem_tarih_saat="#SC"+new SimpleDateFormat("HH:mm:ss_ dd.MM.yyyy", Locale.getDefault()).format(new Date());
        bleService.writeReadData(sistem_tarih_saat);
        cihazKontrol();
    }

    public interface OnStringChangeListener
    {
        public void onStringChanged(String newValue);
    }

    public class ObservableString
    {
        private OnStringChangeListener listener;

        private String value;

        public void setOnStringChangeListener(OnStringChangeListener listener)
        {
            this.listener = listener;
        }

        public String get()
        {
            return value;
        }

        public void set(String value)
        {
            this.value = value;

            if(listener != null)
            {
                listener.onStringChanged(value);
            }
        }
        ObservableString(String value){
            this.value=value;
        }
    }


    private final BroadcastReceiver mBleUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            switch (action) {
                case BLEService.ACTION_BLESCAN_CALLBACK:
                    // Arama düğmesini devre dışı bırak ve bağlan düğmesini etkinleştir
                    break;

                case BLEService.ACTION_CONNECTED:
                    /*bir GATT_CONNECTED alıyoruz */
                    /* Capsense bildirimleri gönderirken yapılacak işlem */
                    if (!mConnectState) {
                        mConnectState = true;
                        Log.d(TAG, "Connected to Device");

                    }
                    break;
                case BLEService.ACTION_DISCONNECTED:
                    // Bağlantıyı kes, svc'yi keşfet, char düğmesini keşfet ve arama düğmesini etkinleştir

                    // LED ve CapSense anahtarlarını kapatma ve devre dışı bırakma
                    led_switch.setChecked(false);
                    led_switch.setEnabled(false);
                    button0.setChecked(false);
                    button1.setChecked(false);
                    led_switch2.setChecked(false);
                    led_switch2.setEnabled(false);
                    ogeler.setVisibility(View.GONE);


                    mConnectState = false;
                    Log.d(TAG, "Disconnected");
                    break;
                case BLEService.ACTION_SERVICES_DISCOVERED:

                    // LED ve CapSense anahtarlarını etkinleştirme
                    led_switch.setEnabled(true);
                    led_switch2.setEnabled(true);
                    loading_panel.setVisibility(View.GONE);
                    ogeler.setVisibility(View.VISIBLE);
                    try {
                        cihazKontrol();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                    Log.d(TAG, "Services Discovered");
                    break;
                case BLEService.CONNECTION_FAILED:

                    ble_eslestir.setChecked(false);
                    loading_panel.setVisibility(View.GONE);
                    baglantiBasarisizUyari();

                    Log.d(TAG, "Services Discovered");
                    break;
                case BLEService.STRING_RECEIVED:

                    if(bleService.okunanData.contains("#SAAT_BILGISI")) {
                        String saat_data = bleService.okunanData;
                        String tarih_data=saat_data;
                        saat_data=saat_data.substring(14,22);
                        currentTime.setText(saat_data);

                        //String tarih_data=bleService.writeReadData("#SAAT_BILGISI");
                        tarih_data=tarih_data.substring(23);
                        currentDate.setText(tarih_data);

                    }
                    else if(bleService.okunanData.contains("#PIL_SEVIYESI")) {
                        String pil_data = bleService.okunanData;
                        int battery=Integer.valueOf(pil_data.substring(16,17)+pil_data.substring(18,19));
                        battery*=2;
                        batteryLevel.setText(battery+"%");
                        if(battery>=80){
                            batteryColor.setBackground(getResources().getDrawable(R.drawable.battery_green));
                        }
                        else if(battery>=65){
                            batteryColor.setBackground(getResources().getDrawable(R.drawable.battery_yellow));
                        }
                        else {
                            batteryColor.setBackground(getResources().getDrawable(R.drawable.battery_red));
                        }
                        try {
                            bleService.writeReadData("#SAAT_BILGISI");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    break;
                case BLEService.ACTION_DATA_RECEIVED:
                    // Bu, bir bildirim veya okuma tamamlandıktan sonra çağrılır
                    // LED anahtar ayarını kontrol edin
                    if(bleService.getLedSwitchState()){
                        led_switch.setChecked(true);

                    } else {
                        led_switch.setChecked(false);
                    }
                    if(bleService.getLed2SwitchState()){
                        led_switch2.setChecked(true);


                    } else {
                        led_switch2.setChecked(false);

                    }

                    if (bleService.getButtonSwitchState()){

                        button0.setChecked(true);
                    }else{

                        button0.setChecked(false);
                    }

                    if (bleService.getButton2SwitchState()){

                        button1.setChecked(true);
                    }else{

                        button1.setChecked(false);
                    }
                    Connected = true;


                default:
                    break;

            }
        }
    };
}

