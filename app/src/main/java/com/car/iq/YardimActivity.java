package com.car.iq;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class YardimActivity extends AppCompatActivity {

    String baslik="";
    String govde="";
    SessionManagement session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yardim);

        session = new SessionManagement(getApplicationContext());

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
    public void aracBilgisi(View view){
        baslik="Araç Bilgisi";
        govde="\nBu komut sayesinde araç ekleyebilir, araç seçebilir, ve aracınızı silebilirsiniz.\n\nEn fazla 20 adet araç eklenebilir.\n\nAraç eklendikten sonra bilgileri değiştirilemez. Bunun yerine aracı silip yeni bir araç eklemeniz gerekmektedir.";
        uyarı_olustur();
    }
    public void olcumYap(View view){
        baslik="Ölçüm Yap";
        govde="\nBu komut sayesinde Car-IQ cihazınızdan gelen verileri görüntüleyip kaydedebilirsiniz.\n\nGünde 1 adet kayıt saklanmaktadır.\n\nAynı gün içerisinde ikinci bir kayıt yapılırsa ilk yapılan kayıt silinir.";
        uyarı_olustur();
    }
    public void analizYap(View view){
        baslik="Analiz Yap";
        govde="\nBu komut sayesinde daha önce yapmış olduğunuz ölçümlerin analizini görüntüleyebilirsiniz.";
        uyarı_olustur();
    }
    public void grafikleriGoster(View view){
        baslik="Grafikleri Göster";
        govde="\nBu komut sayesinde yapmış olduğunuz son 30 ölçüme ait grafikleri görüntüleyebilirsiniz.\n\nGörüntülenen gazı değiştirmek için ekranı sağa-sola kaydırın.";
        uyarı_olustur();
    }
    public void tsBilgi(View view){
        baslik="Teknik Servise Bilgi Ver";
        govde="\nBu komut sayesinde teknik servisinize aracınızın ölçümlerini ve bu ölçümlere ait grafikleri mail olarak yollayabilirsiniz.\n\nEğer aracınızı eklerken bir teknik servis mail adresi girmemişseniz bu bilgiler Bakas Bilişim'e iletilecektir. Aracınızın plakası ile Bakas Bilişim'den talep edebilirsiniz.";
        uyarı_olustur();
    }
    public void kullaniciIslemleri(View view){
        baslik="Kullanıcı İşlemleri";
        govde="\nBu komut sayesinde uygulamaya ilk girişinizde verdiğiniz kullanıcı bilgilerini görüntüleyebilir ve düzenleyebilirsiniz.";
        uyarı_olustur();
    }
    public void cariqIslemleri(View view){
        baslik="Car-IQ İşlemleri";
        govde="\nBu komut sayesinde Car-IQ cihazınızın pil seviyesini görüntüleyebilir, tarih ve saat senkronizasyonu yapabilirsiniz.";
        uyarı_olustur();
    }

    public void uyarı_olustur(){
        AlertDialog.Builder builder = new AlertDialog.Builder(YardimActivity.this, R.style.AlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle(baslik);
        builder.setMessage(govde);

        builder.setNegativeButton("Kapat", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void hakkinda_buton(View view) {
        startActivity(new Intent(this, HakkindaActivity.class));
    }


    public void anasayfaa_buton(View view)
    {
        this.finish();
    }

}
