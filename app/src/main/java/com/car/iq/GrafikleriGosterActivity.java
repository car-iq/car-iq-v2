package com.car.iq;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;

public class GrafikleriGosterActivity extends FragmentActivity {


    DatabaseHelper db;//Database İşlem sınıfı tanımı
    ArrayList<String> listItem;//Veritabanından çekilen verilerin tutulacağı arraylist
    float[] dataArray;//Arraylist in aktarılacağı dizi
    SessionManagement session;
    String plaka="";
    TextView gasText;
    ScrollView scrollView;
    private static final int NUM_PAGES = 4;
    private ViewPager mPager;
    private PagerAdapter pagerAdapter;
    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafikleri_goster);

        extras = getIntent().getExtras();


        mPager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(pagerAdapter);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        db=new DatabaseHelper(this);
        session = new SessionManagement(getApplicationContext());
        plaka=session.getPlaka().get(SessionManagement.KEY_PLAKA);
        //Arraylist tanımı
        listItem=new ArrayList<>();
        listItem.clear();

        gasText=findViewById(R.id.gas_name_text);
        scrollView=findViewById(R.id.scrollViewx);

        mPager.setOffscreenPageLimit(NUM_PAGES);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        gasText.setText("CO₂ vol%");
                        break;
                    case 1:

                        gasText.setText("O₂ vol%");
                        break;
                    case 2:

                        gasText.setText("CO ppm");
                        break;
                    case 3:

                        gasText.setText("NOₓ ppm");
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }


        if (extras != null) {
            if(Integer.parseInt(extras.getString("key"))==1){
                final Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getScreenshot();
                        GrafikleriGosterActivity.this.finish();
                    }
                }, 100);
            }
        }

    }




    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    getData("co2");
                    return  GrafikFragment.newInstance(dataArray,"co2");
                case 1:
                    getData("o2");
                    return  GrafikFragment.newInstance(dataArray,"o2");
                case 2:
                    getData("co");
                    return  GrafikFragment.newInstance(dataArray,"co");
                case 3:
                    getData("nox");
                    return  GrafikFragment.newInstance(dataArray,"nox");
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }


    //Grafiğin Verisinin Eklenmesi
    private ArrayList<Entry> dataValues(){
        ArrayList<Entry> data=new ArrayList<>();
        for(int i=0;i<30;i++)
        {
            data.add(new Entry(i+1,dataArray[i]));
        }

        return data;
    }


    //Veritabanından son 30 günün verisini alan fonksiyon
    private void getData(String gasName) {
        Cursor cursor =db.getGasData(gasName,plaka);
        listItem.clear();
        while (cursor.moveToNext()){
            listItem.add(cursor.getString(0));
        }
        //Alınan verilerin Arraylist ten data array dizisine atanması
        dataArray=new float[30];
        for(int i=0;i<30;i++){
            dataArray[i]=Float.valueOf("0");
        }
        if(gasName.equals("co2")||gasName.equals("o2")){
            for(int i =0;i<listItem.size();i++)
            {
                dataArray[i]=Float.valueOf(listItem.get(i))/100;
            }
        }
        else {
            for(int i =0;i<listItem.size();i++)
            {
                dataArray[i]=Float.valueOf(listItem.get(i));
            }
        }
    }
    public void getScreenshot()
    {
        for(int i=0;i<4;i++){
            mPager.setCurrentItem(i,false);
            getBitmap();

        }
        this.finish();


    }
    public void DownloadBitMap2(ScrollView iv,String veri)
    {
        ImageGenerator ig=new ImageGenerator();
        ig.downloadData2(iv,veri);
        Log.e("callPhone: ", "permission" );
    }
    public void getBitmap(){
        String veri="";
        switch (mPager.getCurrentItem()){
            case 0:
                veri="CO2";
                break;
            case 1:
                veri="O2";
                break;
            case 2:
                veri="CO";
                break;
            case 3:
                veri="NOx";
                break;
            default:
                break;
        }
        DownloadBitMap2(scrollView,veri);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (extras != null) {
            if(Integer.parseInt(extras.getString("key"))==1){
             //anaekran_buton();
            }
        }

    }
    public void anaekran_button(View view){
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();
    }


}
