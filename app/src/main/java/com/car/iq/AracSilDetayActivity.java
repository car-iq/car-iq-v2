package com.car.iq;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class AracSilDetayActivity extends AppCompatActivity {

    SessionManagement session;
    DatabaseHelper db;
    String plaka;
    EditText plakaedit;
    EditText markaedit;
    EditText modeledit;
    EditText yiledit;
    EditText tsmailedit;
    String sessionPlaka;
    TextView plakaText;
    TextView co2;
    TextView co;
    TextView o2;
    TextView nox;
    TextView tariharaligi;
    boolean esit;
    AlertDialog.Builder builder;
    ScrollView scrollView;
    AlertDialog dialog;
    ProgressDialog pDialog;
    String dialogResult="";
    //---------------------------------

    ArrayList<String> listItem;//Veritabanından çekilen verilerin tutulacağı arraylist
    float[] dataArray;//Arraylist in aktarılacağı dizi


    TextView gasText;
    ScrollView scrollViewx;
    private static final int NUM_PAGES = 4;
    private ViewPager mPager;
    private PagerAdapter pagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arac_sil_detay);

        db=new DatabaseHelper(this);

        session = new SessionManagement(getApplicationContext());
        sessionPlaka=session.getPlaka().get(SessionManagement.KEY_PLAKA);

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            plaka = extras.getString("key");
        }

        plakaedit=findViewById(R.id.plaka_edittext);
        markaedit=findViewById(R.id.marka_edittext);
        modeledit=findViewById(R.id.model_edittext);
        yiledit=findViewById(R.id.yil_edittext);
        tsmailedit=findViewById(R.id.tsmail_edittext);

        co2=findViewById(R.id.co2_value);
        o2=findViewById(R.id.o2_value);
        co=findViewById(R.id.co_value);
        nox=findViewById(R.id.nox_value);
        tariharaligi=findViewById(R.id.tarihAraligi);
        plakaText=findViewById(R.id.plaka_value);


        Arac arac=db.aracDetayAl(plaka);

        if(sessionPlaka.equals(plaka)){
            esit=true;

        }
        else{
            esit=false;
        }

        plakaedit.setText(arac.plaka);
        markaedit.setText(arac.marka);
        modeledit.setText(arac.model);
        if(arac.yil!=0){
            yiledit.setText(String.valueOf(arac.yil));
        }
        tsmailedit.setText(arac.tsmail);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }

    }

    public void silButon(View view)
    {

        if(!db.veriVarMi(plaka)){
            db.aracSil("plaka", plaka);
            anasayfa();

        }
        else {
            builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
            builder.setCancelable(true);
            builder.setTitle("Uyarı");
            builder.setMessage("Silmek istediğiniz araca ait ölçüm verileri bulunmaktadır.\nSilmeden önce bu verileri mail olarak almak ister misiniz?");
            builder.setPositiveButton("Hayır",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialogResult="hayir";
                            dialog.dismiss();
                        }
                    });
            builder.setNegativeButton("Evet",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialogResult="evet";
                            dialog.dismiss();
                        }

                    });
            builder.setNeutralButton("Vazgeç",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialogResult="vazgec";
                            dialog.dismiss();
                        }
                    });

            dialog = builder.create();
            dialog.show();

            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run()
                        {
                            gonder();
                        }
                    }, 200);
                }
            });
        }
    }

    public void iptalButon(View view){
        this.finish();
    }

    private void sendEmail(String plaka){
        Mail mail =new Mail(this);
        mail.message="Sayın "+session.getUserDetails().get(SessionManagement.KEY_NAME)+"\n"+
                plaka+" plakalı aracınızın emisyon verisi ekte EXCEL ile açabileceğiniz şekilde bilginize sunulmuştur. Hızlı bir bilgi edinme amacına yönelik olarak son 30 ölçümün grafiği de ayrıca aşağıda verilmiştir. Bu bilgiyi saklayarak, aracınız ile ilgili bakım işlemlerinde ileride kullanmanız mümkündür.";

        mail.email=session.getUserDetails().get(SessionManagement.KEY_EMAIL);
        mail.subject="BAKAS BILISIM Car-IQ ver.1.0 "+plaka+" plakalı aracın emisyon verisi:";
        mail.execute();
    }

    private void anasayfa(){
        if (esit) {
            session.setPlaka("");
        }
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();


    }

    private void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("Veriler.csv", Context.MODE_PRIVATE),"windows-1254");
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "Dosya yazması başarısız: " + e.toString());
        }
    }

    public void DownloadBitMap(ScrollView iv)
    {
        ImageGenerator ig=new ImageGenerator();
        ig.downloadData(iv);
        Log.e("callPhone: ", "permission" );
    }
    //--------------------------------------





    public void grafik_al()
    {
        Intent i = new Intent(AracSilDetayActivity.this, GrafikleriGosterActivity.class);
        i.putExtra("key","1");
        startActivity(i);


    }

    private final BroadcastReceiver mailUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            switch (action) {
                case "Mail_Gonderildi":
                    pDialog.dismiss();
                    anasayfa();
                    break;


                default:
                    break;

            }
        }
    };

    @Override
    protected void onResume() {
        final IntentFilter filter = new IntentFilter();
        filter.addAction("Mail_Gonderildi");

        registerReceiver(mailUpdateReceiver, filter);

        super.onResume();
    }
    @Override
    protected void onPause() {

        unregisterReceiver(mailUpdateReceiver);

        super.onPause();
    }

    public void gonder()
    {
        switch (dialogResult){
            case "vazgec":
                anasayfa();
                break;
            case "hayir":
                db.aracSil("plaka", plaka);
                Handler handler2 = new Handler();
                handler2.postDelayed(new Runnable() {
                    public void run() {
                        anasayfa();
                    }
                }, 1000);   //1 seconds
                break;
            case "evet":
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Mail gönderiliyor...");
                pDialog.show();
                grafik_al();
                writeToFile(db.mailVeriler(plaka),this);

                Handler handler1 = new Handler();
                handler1.postDelayed(new Runnable() {
                    public void run() {
                        sendEmail(plaka);
                    }
                }, 2000);   //2 seconds

                break;
            default:
                break;
        }

    }

}

