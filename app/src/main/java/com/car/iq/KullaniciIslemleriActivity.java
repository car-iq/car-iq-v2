package com.car.iq;

import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class KullaniciIslemleriActivity extends AppCompatActivity {


    DatabaseHelper db;//Database İşlem sınıfı tanımı
    SessionManagement session;
    EditText isim;
    EditText telefon;
    EditText email;
    TextView isim_hata;
    TextView telefon_hata;
    TextView email_hata;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kullanici_islemleri);


        db=new DatabaseHelper(this);

        //activity_anasayfa da ki itemlerin tanımlanması
        Toolbar toolbar=findViewById(R.id.toolbar);
        isim=findViewById(R.id.isim_edittext);
        telefon=findViewById(R.id.telefon_edittext);
        email=findViewById(R.id.mail_edittext);
        isim_hata=findViewById(R.id.isim_hata);
        telefon_hata=findViewById(R.id.telefon_hata);
        email_hata=findViewById(R.id.mail_hata);

        //Activity toolbarının dafault yerine kendi toolbarımız ayarlanması
        setSupportActionBar(toolbar);
        PhoneNumberUtils.formatNumber(telefon.getText().toString());

        session = new SessionManagement(getApplicationContext());

        isim.setText(session.getUserDetails().get(SessionManagement.KEY_NAME));
        telefon.setText(remove_first_two_char(session.getUserDetails().get(SessionManagement.KEY_PHONE)));
        email.setText(session.getUserDetails().get(SessionManagement.KEY_EMAIL));
    }

    public void kaydet(View view)
    {
        if (!isValidEmail(email.getText()))
        {
            email_hata.setVisibility(View.VISIBLE);
        }
        else
        {
            email_hata.setVisibility(View.INVISIBLE);
        }
        if (telefon.getText().length()<15)
        {
            telefon_hata.setVisibility(View.VISIBLE);
        }
        else
        {
            telefon_hata.setVisibility(View.INVISIBLE);
        }
        if(isValidEmail(email.getText())&& telefon.getText().length()>=15)
        {
            String sifre=String.valueOf(telefon.getText().charAt(10))+telefon.getText().charAt(11)+telefon.getText().charAt(13)+telefon.getText().charAt(14);
            /*if(db.insertKullanici(isim.getText().toString(),telefon.getText().toString(),email.getText().toString(),sifre)){
                KullaniciGirisActivity.this.finish();
                Toast.makeText(KullaniciGirisActivity.this,"Giriş Başarılı",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(KullaniciGirisActivity.this, AnasayfaActivity.class));

            }
            else{
                Toast.makeText(KullaniciGirisActivity.this,"Veri Eklenemedi",Toast.LENGTH_SHORT).show();
            }*/

            session.createLoginSession(isim.getText().toString(),telefon.getText().toString(),email.getText().toString(),sifre);
            session.setPlaka("");
            Intent i = new Intent(getApplicationContext(), AnasayfaActivity.class);
            startActivity(i);
            finish();

        }

    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public String removeMask(String string)
    {
        String result="0";
        for(int i=0;i<string.length();i++)
        {
            if(string.charAt(i)!=' '&&string.charAt(i)!='('&&string.charAt(i)!=')')
            {
                result+=string.charAt(i);
            }
        }
        return result;
    }

    public void ana_sayfa_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();
    }

    public String remove_first_two_char(String string)
    {
        String result="";
        for(int i=3;i<string.length();i++)
        {
            result+=string.charAt(i);
        }
        return result;
    }
}
