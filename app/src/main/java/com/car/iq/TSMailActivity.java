package com.car.iq;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class TSMailActivity extends AppCompatActivity {


    DatabaseHelper db;
    SessionManagement session;
    ScrollView scrollView;
    String plaka;
    ProgressDialog Dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t_s_mail);

        scrollView=findViewById(R.id.scroll);

        db=new DatabaseHelper(this);

        session = new SessionManagement(getApplicationContext());

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        plaka=session.getPlaka().get(SessionManagement.KEY_PLAKA);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }

    }
    public void gonderButon(View view)
    {
        Dialog = new ProgressDialog(this);
        Dialog.setMessage("Mail gönderiliyor...");
        Dialog.show();
        grafik_al();
        writeToFile(db.mailVeriler(plaka), TSMailActivity.this);

        Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            public void run() {
                sendEmail();
            }
        }, 2000);   //5 seconds

        /*Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            public void run() {
                anasayfa_buton();
            }
        }, 5000);   //5 seconds*/
    }
    public void anasayfa_buton()
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();
    }
    public void anasayfa_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();
    }

    public void sendEmail(){

        Mail mail =new Mail(this);
        mail.message="Teknik Servis Uzmanının Dikkatine\n"+
                plaka +" plakalı aracın emisyon verisi ekte EXCEL ile açabileceğiniz şekilde bilginize sunulmuştur. Hızlı bir bilgi edinme amacına yönelik olarak son 30 ölçümün grafiği de ayrıca aşağıda verilmiştir.";
        mail.subject="BAKAS BILISIM Car-IQ ver.1.0 "+plaka+" plakalı aracın emisyon verisi:";

        String tsmail=db.getTSMail(session.getPlaka().get(SessionManagement.KEY_PLAKA));
        if(tsmail.isEmpty()||tsmail==""){
            tsmail="info@compositeware.com";
        }
        mail.email=tsmail;
        mail.execute();
    }


    private void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("Veriler.csv", Context.MODE_PRIVATE),"windows-1254");
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "Dosya yazması başarısız: " + e.toString());
        }
    }

    public void grafik_al()
    {
        Intent i = new Intent(TSMailActivity.this, GrafikleriGosterActivity.class);
        i.putExtra("key","1");
        startActivity(i);


    }

    @Override
    protected void onResume() {
        final IntentFilter filter = new IntentFilter();
        filter.addAction("Mail_Gonderildi");

        registerReceiver(mailUpdateReceiver, filter);

        super.onResume();
    }
    @Override
    protected void onPause() {

        unregisterReceiver(mailUpdateReceiver);

        super.onPause();
    }

    private final BroadcastReceiver mailUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            switch (action) {
                case "Mail_Gonderildi":
                    Dialog.dismiss();
                    anasayfa_buton();
                    break;


                default:
                    break;

            }
        }
    };

}