package com.car.iq;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class HakkindaActivity extends AppCompatActivity {

    SessionManagement session;
    DatabaseHelper db;
    String plaka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hakkinda);

        db=new DatabaseHelper(this);

        session = new SessionManagement(getApplicationContext());
        plaka=session.getPlaka().get(SessionManagement.KEY_PLAKA);



    }


    public void ana_sayfa_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();
    }

    public void self_test_buton(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogStyle);
        builder.setCancelable(true);
        builder.setTitle("Uyarı");
        builder.setMessage("Seçili araca ait 500 test verisi yüklenecektir. Bu işlem kayıtlı verilerinizin silinmesine sebep olur. Devam etmek istiyor musunuz?");
        builder.setPositiveButton("Hayır",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.setNegativeButton("Evet",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        db.tumVerileriSil(plaka);

                        for(int i=1;i<=500;i++){
                            db.insertOlcum(plaka,getCalculatedDate((i-1)),i,i,i,i);
                        }
                        dialog.dismiss();
                    }
                });


        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                makeToast();
                anasayfa();
            }
        });

    }

    public static String getCurrentTimeStamp(){
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String currentDateTime = dateFormat.format(new Date()); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }


    }
    public void makeToast(){
        Toast.makeText(HakkindaActivity.this,"Veriler Eklendi",Toast.LENGTH_SHORT);
    }

    public static String getCalculatedDate(int days) {
        String dateFormat="yyyy-MM-dd";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        cal.add(Calendar.DAY_OF_YEAR, days);
        return s.format(new Date(cal.getTimeInMillis()));
    }

    private void anasayfa(){

        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();


    }

}
