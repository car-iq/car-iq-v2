package com.car.iq;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class OlcumYapActivity extends AppCompatActivity {

    TextView plakaText;
    TextView co2;
    TextView o2;
    TextView co;
    TextView nox;
    DatabaseHelper db;
    SessionManagement session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_olcum_yap);

        db=new DatabaseHelper(this);

        session = new SessionManagement(getApplicationContext());

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        co2=findViewById(R.id.co2_value);
        o2=findViewById(R.id.o2_value);
        co=findViewById(R.id.co_value);
        nox=findViewById(R.id.nox_value);
        plakaText=findViewById(R.id.plaka_value);
        String plaka=session.getPlaka().get(SessionManagement.KEY_PLAKA);
        plakaText.setText(plaka);

    }

    public void olcum_kaydet(View view)
    {

        if(db.bugunOlcumYapildiMi(getCurrentTimeStamp())){
            db.veriSilTarihle("tarih",getCurrentTimeStamp());
            if(db.insertOlcum(session.getPlaka().get(SessionManagement.KEY_PLAKA),getCurrentTimeStamp(),(int)(Double.parseDouble(co2.getText().toString())*100),(int)(Double.parseDouble(o2.getText().toString())*100),(int)(Double.parseDouble(co.getText().toString())),(int)(Double.parseDouble(nox.getText().toString())))){
                Toast.makeText(this,"Ölçüm Kaydedildi",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, AnasayfaActivity.class));
                this.finish();

            }
            else{
                Toast.makeText(this,"Veri Eklenemedi",Toast.LENGTH_SHORT).show();
            }
        }
        else{
            if(db.veri500mu(session.getPlaka().get(SessionManagement.KEY_PLAKA))){
                db.eskiOlcumuSil(session.getPlaka().get(SessionManagement.KEY_PLAKA));
                if(db.insertOlcum(session.getPlaka().get(SessionManagement.KEY_PLAKA),getCurrentTimeStamp(),(int)(Double.parseDouble(co2.getText().toString())*100),(int)(Double.parseDouble(o2.getText().toString())*100),(int)(Double.parseDouble(co.getText().toString())),(int)(Double.parseDouble(nox.getText().toString())))){
                    Toast.makeText(this,"Ölçüm Kaydedildi",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, AnasayfaActivity.class));
                    this.finish();
                }
                else{
                    Toast.makeText(this,"Veri Eklenemedi",Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                if(db.insertOlcum(session.getPlaka().get(SessionManagement.KEY_PLAKA),getCurrentTimeStamp(),(int)(Double.parseDouble(co2.getText().toString())*100),(int)(Double.parseDouble(o2.getText().toString())*100),(int)(Double.parseDouble(co.getText().toString())),(int)(Double.parseDouble(nox.getText().toString())))){
                    Toast.makeText(this,"Ölçüm Kaydedildi",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, AnasayfaActivity.class));
                    this.finish();
                }
                else{
                    Toast.makeText(this,"Veri Eklenemedi",Toast.LENGTH_SHORT).show();
                }
            }

        }


    }

    public static String getCurrentTimeStamp(){
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String currentDateTime = dateFormat.format(new Date()); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public void anaekran_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();
    }

    public void random_numbers(View view){
        Random random=new Random();
        DecimalFormat df=new DecimalFormat("#.00", DecimalFormatSymbols.getInstance(Locale.US));
        co2.setText(df.format(40 * random.nextDouble()));
        o2.setText(df.format(25 * random.nextDouble()));
        nox.setText(String.valueOf( random.nextInt(3000)));
        co.setText(String.valueOf( random.nextInt(3000)));
    }
}
