package com.car.iq;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class AracDetayActivity extends AppCompatActivity {

    SessionManagement session;
    DatabaseHelper db;
    String plaka;
    EditText plakaedit;
    EditText markaedit;
    EditText modeledit;
    EditText yiledit;
    EditText tsmailedit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arac_detay);

        db=new DatabaseHelper(this);

        session = new SessionManagement(getApplicationContext());

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            plaka = extras.getString("key");
        }

        plakaedit=findViewById(R.id.plaka_edittext);
        markaedit=findViewById(R.id.marka_edittext);
        modeledit=findViewById(R.id.model_edittext);
        yiledit=findViewById(R.id.yil_edittext);
        tsmailedit=findViewById(R.id.tsmail_edittext);



        Arac arac=db.aracDetayAl(plaka);

        plakaedit.setText(arac.plaka);
        markaedit.setText(arac.marka);
        modeledit.setText(arac.model);
        if(arac.yil!=0){
            yiledit.setText(String.valueOf(arac.yil));
        }
        tsmailedit.setText(arac.tsmail);

    }

    public void secButon(View view)
    {
        session.setPlaka(plaka);
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();
    }

    public void iptalButon(View view){
        this.finish();
    }
}
