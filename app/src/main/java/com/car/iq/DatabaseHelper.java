package com.car.iq;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;


public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME="Cariq.db";//Veritabanı Adı
    private static final String VERI_TABLE="Veriler_Table";//Tablo Adı
    private static final String ARACLAR_TABLE="Araclar_Table";//Tablo Adı
    private static final String KULLANICI_TABLE="Kullanici_Table";//Tablo Adı
    private static final String TANILAR_TABLE="Tanilar_Table";//Tablo Adı

    //Sütunlar
    private static final String Veri_no="veri_no";
    private static final String Arac_no="arac_no";
    private static final String Tarih="tarih";
    private static final String CO2="co2";
    private static final String O2="o2";
    private static final String CO="co";
    private static final String NOx="nox";
    private static final String Plaka="plaka";
    private static final String Marka="marka";
    private static final String Model="model";
    private static final String Yil="yil";
    private static final String Teknik_servis_mail="teknik_servis_mail";
    private static final String Ad_soyad="ad_soyad";
    private static final String Telefon="telefon_no";
    private static final String Mail="mail";
    private static final String Sifre="sifre";
    private static final String Hata_kodu="hata_kodu";

    //Create table query si
    private static final String CREATE_TABLE1="CREATE TABLE "+ VERI_TABLE+" ("+
            Veri_no + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
            Plaka+ " TEXT, "+
            Tarih+ " TEXT, "+
            CO2+ " INTEGER, "+
            O2+ " INTEGER, "+
            CO+ " INTEGER, "+
            NOx+ " INTEGER "+
            ")";
    private static final String CREATE_TABLE2="CREATE TABLE "+ ARACLAR_TABLE+" ("+
            Arac_no + " INTEGER PRIMARY KEY , "+
            Plaka+ " TEXT, "+
            Marka+ " TEXT, "+
            Model+ " TEXT, "+
            Yil+ " INTEGER, "+
            Teknik_servis_mail+ " TEXT "+
            ")";
    private static final String CREATE_TABLE3="CREATE TABLE "+ KULLANICI_TABLE+" ("+
            Ad_soyad + " TEXT PRIMARY KEY , "+
            Telefon+ " INTEGER, "+
            Mail+ " TEXT, "+
            Sifre+ " INTEGER "+
            ")";
    private static final String CREATE_TABLE4="CREATE TABLE "+ TANILAR_TABLE+" ("+
            Veri_no + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
            Plaka+ " TEXT, "+
            Tarih+ " TEXT, "+
            CO2+ " INTEGER, "+
            O2+ " INTEGER, "+
            CO+ " INTEGER, "+
            NOx+ " INTEGER, "+
            Hata_kodu+ " INTEGER "+
            ")";

    //Constructor
    public DatabaseHelper(Context context)
    {
        super(context,DB_NAME,null,1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE1);//Çalıştırıldığında tablo oluştur komutu
        db.execSQL(CREATE_TABLE2);
        db.execSQL(CREATE_TABLE3);
        db.execSQL(CREATE_TABLE4);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ VERI_TABLE);//Değişiklik durumunda tabloyu yeniden oluştur komutu
        db.execSQL("DROP TABLE IF EXISTS "+ ARACLAR_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+ KULLANICI_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+ TANILAR_TABLE);
        onCreate(db);
    }


    //araç girişi
    public boolean insertArac(String plaka, String marka, String model, int yil,String servisMail){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues2=new ContentValues();
        contentValues2.put("plaka",plaka);
        contentValues2.put("marka",marka);
        contentValues2.put("model",model);
        contentValues2.put("yil",yil);
        contentValues2.put("teknik_servis_mail",servisMail);
        long result = db.insert(ARACLAR_TABLE,null,contentValues2);

        return result !=-1;
    }

    //araç girişi
    public boolean insertOlcum(String plaka, String tarih, int co2, int o2, int co, int nox){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues3=new ContentValues();
        contentValues3.put("plaka",plaka);
        contentValues3.put("tarih",tarih);
        contentValues3.put("co2",co2);
        contentValues3.put("o2",o2);
        contentValues3.put("co",co);
        contentValues3.put("nox",nox);
        long result = db.insert(VERI_TABLE,null,contentValues3);

        return result !=-1;

    }

    //kullanici girişi
    public boolean insertKullanici(String adsoyad,String telefon, String mail, int sifre){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues3=new ContentValues();
        contentValues3.put("ad_soyad",adsoyad);
        contentValues3.put("telefon_no",telefon);
        contentValues3.put("mail",mail);
        contentValues3.put("sifre",sifre);
        long result = db.insert(KULLANICI_TABLE,null,contentValues3);

        return result !=-1;
    }

    public Cursor araclarOku(){
        SQLiteDatabase db=this.getWritableDatabase();
        String query="SELECT * FROM "+ARACLAR_TABLE;
        Cursor cursor=db.rawQuery(query,null);

        return cursor;
    }
    public ArrayList getPlaka(){
        SQLiteDatabase sqLiteDatabase= this.getReadableDatabase();
        ArrayList<String> arrayList=new ArrayList<>();
        Cursor cursor =sqLiteDatabase.rawQuery("SELECT plaka FROM "+ARACLAR_TABLE,null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            arrayList.add(cursor.getString(cursor.getColumnIndex("plaka")));
            cursor.moveToNext();
        }
        sqLiteDatabase.close();
        return arrayList;
    }

    //veri silme
    public Integer aracSil(String column_name,String thingToDelete){
        SQLiteDatabase db= this.getWritableDatabase();
        int result;
        result= db.delete(ARACLAR_TABLE,column_name+"='" + thingToDelete +"' ;", null);
        result=db.delete(VERI_TABLE,column_name+"='" + thingToDelete +"' ;", null);
        result=db.delete(TANILAR_TABLE,column_name+"='" + thingToDelete +"' ;", null);
        return result;
    }
/*
    //son 30 gün verisi okuma
    public Cursor get30Data(){
        SQLiteDatabase db=this.getWritableDatabase();
        String query="SELECT strftime('%Y-%m-%d',"+ateslenme_zamani+") FROM "+DB_TABLE+" WHERE "+ateslenme_zamani+" >= datetime('now','localtime','start of day','-29 day')";
        Cursor cursor=db.rawQuery(query,null);
        return cursor;
    }*/


    public  boolean plakaZatenVar(String plaka) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "Select plaka from " + ARACLAR_TABLE + " where plaka ='" + plaka+"'";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();

        return true;
    }

    public boolean aracVarMi()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "Select * from " + ARACLAR_TABLE ;
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();

        return true;
    }

    public boolean arac20Mi(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "Select * from " + ARACLAR_TABLE ;
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.getCount() >= 20){
            cursor.close();
            return true;
        }
        cursor.close();

        return false;
    }

    public boolean veri500mu(String plaka){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "Select * from " + VERI_TABLE + " where plaka = ' "+plaka+"'" ;
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.getCount() >= 500){
            cursor.close();
            return true;
        }
        cursor.close();

        return false;
    }

    public boolean bugunOlcumYapildiMi(String tarih){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "Select * from " + VERI_TABLE + " where tarih = '"+tarih+"'" ;
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.getCount() >= 1){
            cursor.close();
            return true;
        }
        cursor.close();

        return false;
    }

    public Integer veriSilTarihle(String column_name,String thingToDelete){
        SQLiteDatabase db= this.getWritableDatabase();
        int result;
        result= db.delete(VERI_TABLE,column_name+"='" + thingToDelete +"' ;", null);

        return result;
    }

    public void eskiOlcumuSil(String plaka){
        SQLiteDatabase db= this.getWritableDatabase();
        String query="DELETE FROM "+VERI_TABLE+" WHERE tarih IN (SELECT tarih FROM "+VERI_TABLE+" where plaka= '"+plaka+"' ORDER BY tarih DESC LIMIT -1 OFFSET 500)";
        db.execSQL(query);
    }

    public Arac aracDetayAl(String plaka){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + ARACLAR_TABLE + " WHERE " + Plaka + "='" + plaka+"'",null);
        cursor.moveToFirst();

        //setting related user info in User Object
        Arac arac = new Arac();
        Arac.plaka=(cursor.getString(cursor.getColumnIndex(Plaka )));
        Arac.marka=(cursor.getString(cursor.getColumnIndex(Marka)));
        Arac.model=(cursor.getString(cursor.getColumnIndex(Model )));
        Arac.yil=(cursor.getInt(cursor.getColumnIndex(Yil )));
        Arac.tsmail=(cursor.getString(cursor.getColumnIndex(Teknik_servis_mail )));

        //close cursor & database
        cursor.close();

        return arac;

    }

    public String ortalamaAl(String column,String plaka)
    {
        String result="";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT Avg("+column+") FROM " + VERI_TABLE + " WHERE " + Plaka + "='" + plaka+"'",null);
        cursor.moveToFirst();

        if(column.equals("co2")||column.equals("o2")) {
            result = String.format("%.2f", Double.parseDouble(cursor.getString(cursor.getColumnIndex("Avg(" + column + ")"))) / 100);
        }
        else {
            result = String.format("%.0f", Double.parseDouble(cursor.getString(cursor.getColumnIndex("Avg(" + column + ")"))));
        }

        //close cursor & database
        cursor.close();

        return result;
    }

    public String tarihAraligiAl(String plaka)
    {
        String ilkTarih,sonTarih;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT tarih FROM " + VERI_TABLE + " WHERE " + Plaka + "='" + plaka+"'",null);
        cursor.moveToFirst();


        ilkTarih=cursor.getString(cursor.getColumnIndex("tarih"));
        cursor.moveToLast();
        sonTarih=cursor.getString(cursor.getColumnIndex("tarih"));

        //close cursor & database
        cursor.close();

        return ilkTarih+"  -  "+sonTarih;

    }


    public String getTSMail(String plaka)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT teknik_servis_mail FROM " + ARACLAR_TABLE + " WHERE " + Plaka + "='" + plaka+"'",null);
        cursor.moveToFirst();


        String mail=cursor.getString(cursor.getColumnIndex("teknik_servis_mail"));


        //close cursor & database
        cursor.close();

        return mail;

    }


    public boolean veriVarMi(String plaka)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "Select * from " + VERI_TABLE +" WHERE " + Plaka + "='" + plaka+"'";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();

        return true;
    }

    public String mailVeriler(String plaka){
        String text="BAKAS BILISIM\nCar-IQ\nver.2.0\n"+plaka+"\nPLAKALI ARACIN EMISYON VERISI:\nTarih,CO2,O2,CO,NOx\n";

        SQLiteDatabase db = this.getWritableDatabase();
        DecimalFormat df=new DecimalFormat("#.00", DecimalFormatSymbols.getInstance(Locale.US));
        Cursor cursor = db.rawQuery("SELECT * FROM " + VERI_TABLE + " WHERE " + Plaka + "='" + plaka+"'",null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            text=text+cursor.getString(cursor.getColumnIndex("tarih"))+",";
            text=text+df.format(Double.parseDouble(cursor.getString(cursor.getColumnIndex("co2")))/100)+",";
            text=text+df.format(Double.parseDouble(cursor.getString(cursor.getColumnIndex("o2")))/100)+",";
            text=text+df.format(Double.parseDouble(cursor.getString(cursor.getColumnIndex("co"))))+",";
            text=text+df.format(Double.parseDouble(cursor.getString(cursor.getColumnIndex("nox"))));
            text=text+"\n";
            cursor.moveToNext();
        }
        cursor.close();

        return text;
    }

    public String mailVeriler2(String plaka){
        String text="BAKAS BILISIM\nCar-IQ\nver.2.0\n"+plaka+"\nPLAKALI ARACIN MOTOR ANALIZ VERISI:\nTarih,CO2,O2,CO,NOx,Hata\n";

        SQLiteDatabase db = this.getWritableDatabase();
        DecimalFormat df=new DecimalFormat("#.00", DecimalFormatSymbols.getInstance(Locale.US));
        Cursor cursor = db.rawQuery("SELECT * FROM " + TANILAR_TABLE + " WHERE " + Plaka + "='" + plaka+"'",null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            text=text+cursor.getString(cursor.getColumnIndex("tarih"))+",";
            text=text+df.format(Double.parseDouble(cursor.getString(cursor.getColumnIndex("co2")))/100)+",";
            text=text+df.format(Double.parseDouble(cursor.getString(cursor.getColumnIndex("o2")))/100)+",";
            text=text+df.format(Double.parseDouble(cursor.getString(cursor.getColumnIndex("co"))))+",";
            text=text+df.format(Double.parseDouble(cursor.getString(cursor.getColumnIndex("nox"))))+",";
            text=text+hataAdiBul(Integer.parseInt(cursor.getString(cursor.getColumnIndex("hata_kodu"))));
            text=text+"\n";
            cursor.moveToNext();
        }
        cursor.close();

        return text;
    }

    public void tumVerileriSil(String plaka){
        SQLiteDatabase db= this.getWritableDatabase();
        String query="Delete FROM " + VERI_TABLE + " WHERE " + Plaka + "='" + plaka+"'";
        db.execSQL(query);
    }
    public void tumTanilariSil(String plaka){
        SQLiteDatabase db= this.getWritableDatabase();
        String query="Delete FROM " + TANILAR_TABLE + " WHERE " + Plaka + "='" + plaka+"'";
        db.execSQL(query);
    }

    public Cursor getGasData(String gasName,String plaka){
        SQLiteDatabase db=this.getWritableDatabase();
        String query="SELECT "+gasName+" FROM (SELECT * FROM Veriler_Table WHERE plaka='"+plaka+"' ORDER BY tarih DESC LIMIT 30) ORDER BY tarih ASC ";
        Cursor cursor=db.rawQuery(query,null);
        return cursor;
    }

    public int sonOlcumAl(String column,String plaka)
    {
        int result;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT "+column+" FROM " + VERI_TABLE + " WHERE " + Plaka + "='" + plaka+"'ORDER BY tarih DESC LIMIT 1",null);
        cursor.moveToFirst();

        result = Integer.parseInt(cursor.getString(cursor.getColumnIndex(column)));

        //close cursor & database
        cursor.close();

        return result;
    }

    public String sonOlcumTarihiAl(String plaka)
    {
        String ilkTarih;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT tarih FROM " + VERI_TABLE + " WHERE " + Plaka + "='" + plaka+"'ORDER BY tarih DESC LIMIT 1",null);
        cursor.moveToFirst();

        ilkTarih=cursor.getString(cursor.getColumnIndex("tarih"));

        //close cursor & database
        cursor.close();

        return ilkTarih;

    }

    public boolean insertTani(String plaka, String tarih, int co2, int o2, int co, int nox, int hata_kodu){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues4=new ContentValues();
        contentValues4.put("plaka",plaka);
        contentValues4.put("tarih",tarih);
        contentValues4.put("co2",co2);
        contentValues4.put("o2",o2);
        contentValues4.put("co",co);
        contentValues4.put("nox",nox);
        contentValues4.put("hata_kodu",hata_kodu);
        long result = db.insert(TANILAR_TABLE,null,contentValues4);

        return result !=-1;

    }

    public String hataAdiBul(int hata_kodu){
        switch (hata_kodu){
            case 0:
                return "Aracınızın motorunda herhangi bir sorun yoktur";

            case 1:
                return "Katalitik konvertör arızası / kullanım ömrü sonu";

            case 2:
                return "Piston yükseklik ayar hatası";

            case 3:
                return "Emme manifoldu gaz kaçağı";

            case 4:
                return "Yakıt pompası ayarsızlığı / arızası";

            case 5:
                return "Aşırı yüksek zenginlikte hava/yakıt karışımı";

            case 6:
                return "Ayarsız / hatalı yakıt enjeksiyonu";

            case 7:
                return "Emme supabı hatası";

            case 8:
                return "Silindir kapağı sorunu";

            case 9:
                return "Supap kapağı sorunu";

            case 10:
                return "Silindir gövdesi sorunu";

            case 11:
                return "Egzoz manifoldu tıkanıklığı (egzoz borusu tıkanıklığı)";

            case 12:
                return "Hatalı / çatlak motor contaları";

            case 13:
                return "Yağ pompası ayarsızlığı / arızası";

            case 14:
                return "Tıkalı yağ kanalları";

            case 15:
                return "Kirli yakıt filtresi";

            case 16:
                return "Kirli yağ filtresi";

            case 17:
                return "Ateşleme zamanlama hatası";

            case 18:
                return "Ateşleme sistemi hatası (Buji veya Endüksiyon bobini)";

            case 19:
                return "O2 sensör hatası";

            case 20:
                return "Egzoz supabı hatası";

            case 21:
                return "ÇOKLU MOTOR ARIZASI";

            default:
                return "Aracınızın motorunda herhangi bir sorun yoktur";
        }
    }

}