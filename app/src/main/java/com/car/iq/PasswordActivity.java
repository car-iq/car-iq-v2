package com.car.iq;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.HashMap;

public class PasswordActivity extends AppCompatActivity {

    SessionManagement session;
    String pass;
    EditText girilensifre;
    TextView sifrehata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        //sayfada ki itemlerin tanımlanması
        Toolbar toolbar=findViewById(R.id.toolbar);
        girilensifre=findViewById(R.id.sifre_edittext);
        sifrehata=findViewById(R.id.sifre_hata);

        // Session Manager
        session = new SessionManagement(getApplicationContext());

        if(!session.checkLogin()){
         this.finish();
        }

        HashMap<String, String> kullanici = session.getUserDetails();
        pass=kullanici.get(SessionManagement.KEY_PASS);



        //Activity toolbarının dafault yerine kendi toolbarımız ayarlanması
        setSupportActionBar(toolbar);
    }

    public void kaydet(View view)
    {
        if(girilensifre.getText().toString().equals(pass))
        {
            Intent i = new Intent(getApplicationContext(), AnasayfaActivity.class);
            session.setPlaka("");
            startActivity(i);
            finish();
        }
        else {
            sifrehata.setVisibility(View.VISIBLE);
        }

    }
}
