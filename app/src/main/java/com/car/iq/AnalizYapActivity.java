package com.car.iq;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AnalizYapActivity extends AppCompatActivity {

    TextView plakaText;
    TextView co2;
    TextView co;
    TextView o2;
    TextView nox;
    TextView tariharaligi;
    TextView analizsonuc;
    DatabaseHelper db;
    SessionManagement session;
    ScrollView scrollView;
    int hata_kodu=0;
    int co2_value;
    int co_value;
    int o2_value;
    int nox_value;
    int NormCO2=2600,NormO2=1700,NormCO=1700,NormNOX=300
            ,varCO2_1=2200,varCO2_2=2400,varCO2_3=2400,varCO2_4=2600,varCO2_5=2200,varCO2_6=2400,varCO2_7=1800,varCO2_8=2400
            ,varO2_1=1700,varO2_2=1800,varO2_3=2000,varO2_4=2100,varO2_5=1700,varO2_6=1800,varO2_7=2200,varO2_8=1800
            ,varCO_1=1800,varCO_2=2000,varCO_3=2400,varCO_4=1700,varCO_5=1900,varCO_6=1800,varCO_7=2000,varCO_8=2600
            ,varNOX_1=300,varNOX_2=400,varNOX_3=450,varNOX_4=600,varNOX_5=800,varNOX_6=900,varNOX_7=1100,varNOX_8=300,varNOX_9=400,varNOX_10=450;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analiz_yap);

        scrollView=findViewById(R.id.scroll);

        db=new DatabaseHelper(this);

        session = new SessionManagement(getApplicationContext());

        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        co2=findViewById(R.id.co2_value);
        o2=findViewById(R.id.o2_value);
        co=findViewById(R.id.co_value);
        nox=findViewById(R.id.nox_value);
        tariharaligi=findViewById(R.id.tarihAraligi);
        plakaText=findViewById(R.id.plaka_value);
        analizsonuc=findViewById(R.id.analiz_sonuc);


        String ilkTarih,sonTarih;
        String plaka=session.getPlaka().get(SessionManagement.KEY_PLAKA);


        co2_value=db.sonOlcumAl("co2",plaka);
        o2_value=db.sonOlcumAl("o2",plaka);
        co_value=db.sonOlcumAl("co",plaka);
        nox_value=db.sonOlcumAl("nox",plaka);

        co2.setText(String.valueOf(Double.parseDouble(String.valueOf(co2_value))/100));
        o2.setText(String.valueOf(Double.parseDouble(String.valueOf(o2_value))/100));
        co.setText(String.valueOf(co_value));
        nox.setText(String.valueOf(nox_value));

        plakaText.setText(plaka);
        tariharaligi.setText(db.sonOlcumTarihiAl(plaka));
        analizsonuc.setText(hataBul(co2_value,o2_value,co_value,nox_value));
        if(hata_kodu==0){
            analizsonuc.setTextColor(Color.parseColor("#20C50E"));
        }
        else if(hata_kodu>=1&&hata_kodu<=20){
            analizsonuc.setTextColor(Color.parseColor("#CDDC39"));
        }
        else if(hata_kodu==21){
            analizsonuc.setTextColor(Color.parseColor("#F44336"));
        }

        db.insertTani(plaka,getCurrentTimeStamp(),co2_value,o2_value,co_value,nox_value,hata_kodu);


    }

    public void anaekran_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();
    }

    private String hataBul(int co2,int o2,int co,int nox)
    {
        if (co2 < varCO2_1 && co2 >= varCO2_7 && o2 > varO2_1 && o2 <= varO2_2&& co <= NormCO && nox <= NormNOX) {
            hata_kodu=1;
            return "Katalitik konvertör arızası / kullanım ömrü sonu";
        }
        if (co2 < varCO2_2 && co2 >= varCO2_1 && o2 > varO2_2 && o2 <= varO2_3&& co <= NormCO && nox <= NormNOX) {
            hata_kodu=2;
            return "Piston yükseklik ayar hatası";
        }
        if (co2 < varCO2_3 && co2 >= varCO2_1 && o2 > varO2_3 && o2 <= varO2_4&& co <= NormCO && nox <= NormNOX) {
            hata_kodu=3;
            return "Emme manifoldu gaz kaçağı";
        }
        if (co2 < varCO2_4 && co2 >= varCO2_3 && o2 > varO2_4 && o2 <= varO2_7&& co <= NormCO && nox <= NormNOX) {
            hata_kodu=4;
            return "Yakıt pompası ayarsızlığı / arızası";
        }
        if (co2 >= NormCO2 && o2 <= NormO2 &&co > varCO_1 && co <= varCO_5&& nox <= NormNOX) {
            hata_kodu=5;
            return "Aşırı yüksek zenginlikte hava/yakıt karışımı";
        }
        if (co2 >= NormCO2 && o2 <= NormO2 &&co > varCO_2 && co <= varCO_3&& nox <= NormNOX) {
            hata_kodu=6;
            return "Ayarsız / hatalı yakıt enjeksiyonu";
        }
        if (co2 >= NormCO2 && o2 <= NormO2 &&co > varCO_3 && co <= varCO_8&& nox <= NormNOX) {
            hata_kodu=7;
            return "Emme supabı hatası";
        }
        if (co2 >= NormCO2 && o2 <= NormO2 && co <= NormCO && nox > varNOX_1 && nox <= varNOX_2) {
            hata_kodu=8;
            return "Silindir kapağı sorunu";
        }
        if (co2 >= NormCO2 && o2 <= NormO2 && co <= NormCO && nox > varNOX_2 && nox <= varNOX_3) {
            hata_kodu=9;
            return "Supap kapağı sorunu";
        }
        if (co2 >= NormCO2 && o2 <= NormO2 && co <= NormCO && nox > varNOX_3 && nox <= varNOX_4) {
            hata_kodu=10;
            return "Silindir gövdesi sorunu";
        }
        if (co2 >= NormCO2 && o2 <= NormO2 && co <= NormCO && nox > varNOX_4 && nox <= varNOX_5) {
            hata_kodu=11;
            return "Egzoz manifoldu tıkanıklığı (egzoz borusu tıkanıklığı)";
        }
        if (co2 >= NormCO2 && o2 <= NormO2 && co <= NormCO && nox > varNOX_5 && nox <= varNOX_6) {
            hata_kodu=12;
            return "Hatalı / çatlak motor contaları";
        }
        if (co2 >= NormCO2 && o2 <= NormO2 && co <= NormCO && nox > varNOX_6 && nox <= varNOX_7) {
            hata_kodu=13;
            return "Yağ pompası ayarsızlığı / arızası";
        }
        if (co2 >= NormCO2 && o2 <= NormO2 && co <= NormCO && nox > varNOX_7) {
            hata_kodu=14;
            return "Tıkalı yağ kanalları";
        }
        if (co2 >= NormCO2 && o2 <= NormO2 && co > varCO_4 && co <= varCO_6 && nox > varNOX_8 && nox <= varNOX_9) {
            hata_kodu=15;
            return "Kirli yakıt filtresi";
        }
        if (co2 >= NormCO2 && o2 <= NormO2 && co > varCO_5 && co <= varCO_6 && nox > varNOX_9 && nox <= varNOX_3) {
            hata_kodu=16;
            return "Kirli yağ filtresi";
        }
        if (co2 < varCO2_5 && co2 >= varCO2_7 && o2 > varO2_5 && o2 <= varO2_6 && co > varCO_6 && co <= varCO_5 && nox <= NormNOX) {
            hata_kodu=17;
            return "Ateşleme zamanlama hatası";
        }
        if (co2 < varCO2_6 && co2 >= varCO2_5 && o2 > varO2_6 && o2 <= varO2_3 && co > varCO_7 && co <= varCO_3 && nox <= NormNOX) {
            hata_kodu=18;
            return "Ateşleme sistemi hatası (Buji veya Endüksiyon bobini)";
        }
        if (co2 < varCO2_7 && o2 > varO2_7 && co > varCO_8 && nox <= NormNOX) {
            hata_kodu=19;
            return "O2 sensör hatası";
        }
        if (co2 < varCO2_8 && co >= varCO2_1 && o2 > varO2_8 && o2 <= varO2_3 && co <= NormCO && nox > varNOX_10 && nox <= varNOX_4) {
            hata_kodu=20;
            return "Egzoz supabı hatası";
        }
        if (co2 < varCO2_7 && o2 > varO2_7 && co > varCO_8 && nox > varNOX_7) {
            hata_kodu=21;
            return "ÇOKLU MOTOR ARIZASI";
        }
        if (co2 >= varCO2_4 && o2 <= varO2_2 && co <= NormCO && nox <= NormNOX) {
            return "Aracınızın motorunda herhangi bir sorun yoktur";
        }
        else return "Aracınızın motorunda herhangi bir sorun yoktur";
    }

    public static String getCurrentTimeStamp(){
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String currentDateTime = dateFormat.format(new Date()); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }
}
