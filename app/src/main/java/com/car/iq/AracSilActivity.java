package com.car.iq;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

public class AracSilActivity extends AppCompatActivity {

    DatabaseHelper db;//Database İşlem sınıfı tanımı
    ArrayList<String> listItem;//Liste itemlerini tutacak Array List
    ArrayAdapter adapter;// Array list için adapter
    ListView araclist;
    SessionManagement session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arac_sec);

        //Database İşlem sınıfı tanımı
        db=new DatabaseHelper(this);

        session = new SessionManagement(getApplicationContext());

        //Itemlerin tanımlanması
        listItem=new ArrayList<>();
        araclist=findViewById(R.id.arac_list);
        Toolbar toolbar=findViewById(R.id.toolbar);

        //Listenin Temizlenmesi
        listItem.clear();

        //Temizlenen listeye veri ekleme
        viewData();

        //Activity toolbarının dafault yerine kendi toolbarımız ayarlanması
        setSupportActionBar(toolbar);

        araclist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
               /* session.setPlaka(araclist.getItemAtPosition(position).toString());
                finishActivity();*/



                /*AlertDialog.Builder builder = new AlertDialog.Builder(AracSecActivity.this,R.style.AlertDialogStyle);
                builder.setCancelable(true);
                builder.setMessage("Araç Plakası: "+araclist.getItemAtPosition(position).toString());
                builder.setPositiveButton("Seç",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                session.setPlaka(araclist.getItemAtPosition(position).toString());
                                finishActivity();
                            }
                        });
                builder.setNegativeButton("İptal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();*/


                String value=araclist.getItemAtPosition(position).toString();
                Intent i = new Intent(AracSilActivity.this, AracSilDetayActivity.class);
                i.putExtra("key",value);
                startActivity(i);

            }
        });
    }

    //Listeye veri ekleme fonksiyonu
    private void viewData() {
        Cursor cursor =db.araclarOku();//Veritabanından verileri srayla alacak cursor

        //Veri var mı kontrol
        if(cursor.getCount()==0){
            Toast.makeText(this,"Gösterilecek veri yok",Toast.LENGTH_SHORT).show();
        }
        //veri varsa Array list e ekleme
        else{
            while (cursor.moveToNext()){
                listItem.add(cursor.getString(1));
            }
            //Arraylist i listview e uyarlayan adapter
            adapter=new ArrayAdapter<>(this, R.layout.arac_listview,listItem);
            araclist.setAdapter(adapter);
        }
    }

    private void finishActivity(){
        startActivity(new Intent(this, AnasayfaActivity.class));
        this.finish();
    }
}
